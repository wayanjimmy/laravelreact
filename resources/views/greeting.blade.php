@extends('layouts.master')

@section('content')
    <h1>Orang gila</h1>

    <div class="row">
        <div>
            @react_component('Hello', $propsHello)
        </div>
        <div>
            @react_component('HelloAgain')
        </div>
    </div>

@endsection
