<!doctype html>
<html>
    <head>
        <title>laravel</title>
    </head>
    <body>

        @yield('content')

        @include('layouts.footer')
        <script src={{ elixir('js/main.js') }}></script>
    </body>
</html>
