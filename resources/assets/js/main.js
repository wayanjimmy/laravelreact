import React from 'react';
import { render } from 'react-dom';
import Hello from './components/Hello';
import HelloAgain from './components/HelloAgain';

const availableComponents = {
  Hello,
  HelloAgain
};

const componentLoader = () => {

  return window.REACT_COMPONENTS.map((value) => {
    let container = `${value}-container`;
    const Component = availableComponents[value];

    let element = document.getElementById(container);

    if (element) {
      let props = [];
      if (element.dataset.props) {
        props = JSON.parse(element.dataset.props);
      }
      render(<Component {...props}/>, element);
    }

    return {
      container
    };
  });

};

componentLoader();
