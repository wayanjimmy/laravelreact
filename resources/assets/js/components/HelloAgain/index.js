import React, { Component } from 'react';

class HelloAgain extends Component {
  render() {
    return (
      <div>
        <h2>Hello Again!</h2>
      </div>
    );
  }
}

export default HelloAgain;
