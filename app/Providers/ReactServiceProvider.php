<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;
use App\React\LaravelViewBinder;
use App\React\ReactTransformer;

class ReactServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::extend(function ($view) {
            $pattern = $this->createMatcher('react_component');

            return preg_replace($pattern, '<?php echo React::render$2; ?>', $view);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('react', function ($app) {

            $binder = new LaravelViewBinder($app['events'], 'layouts.footer');

            return new ReactTransformer($binder);
        });
    }

    protected function createMatcher($function)
    {
        return '/(?<!\w)(\s*)@' . $function . '(\s*\([\s\S]*?\))/';
    }
}
