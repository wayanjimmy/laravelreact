<?php

namespace App\React;

interface ViewBinder
{
    public function bind($js);
}
