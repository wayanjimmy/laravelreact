<?php

namespace App\React;

/**
 * @author Jimmy
 */
class ReactTransformer
{
    protected $namespace = 'window.REACT_COMPONENTS';

    protected $viewBinder;

    public function __construct(ViewBinder $viewBinder)
    {
        $this->viewBinder = $viewBinder;
    }

    public function render($component, $props = null)
    {
        $this->put($component);

        $props = htmlentities(json_encode($props), ENT_QUOTES);

        return '<div id="' . $component . '-container" data-props="' . $props . '"></div>';
    }

    private function put()
    {
        $components = func_get_args();

        $js = $this->buildJavascriptSyntax($components);

        $this->viewBinder->bind($js);

        return $js;
    }

    private function normalizeComponentName($component)
    {
        return ucfirst($component);
    }

    private function buildNamespaceDeclaration()
    {
        return "{$this->namespace} = $this->namespace || [];";
    }

    private function buildJavascriptSyntax($components)
    {
        $js = $this->buildNamespaceDeclaration();

        foreach ($components as $component) {
            $component = $this->normalizeComponentName($component);
            $js .= "$this->namespace.push('{$component}');";
        }

        return $js;
    }
}
