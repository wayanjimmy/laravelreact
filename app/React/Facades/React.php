<?php

namespace App\React\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @author Jimmy
 */
class React extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'react';
    }
}
