<?php

namespace App\React;

use Illuminate\Contracts\Events\Dispatcher;

/**
 * @author Jimmy
 */
class LaravelViewBinder implements ViewBinder
{
    protected $event;

    protected $views;

    public function __construct(Dispatcher $event, $views)
    {
        $this->event = $event;
        $this->views = str_replace('/', '.', (array) $views);
    }

    public function bind($js)
    {
        foreach ($this->views as $view) {
            $this->event->listen("composing: {$view}", function () use ($js) {
                echo "<script>{$js}</script>";
            });
        }
    }
}
